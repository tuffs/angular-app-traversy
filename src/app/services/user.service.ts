import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { User } from '../models/User';

@Injectable()
export class UserService {
  users: User[];
  data: Observable<Array<any>>;
  constructor() {
    this.users = [
      {
        firstName: "Devon",
        lastName: "Kiss",
        email: "devon@devonkissstudios.com",
        isActive: true,
        registered: new Date('11/28/2017 11:20:00'),
        hide: true,
      },
      {
        firstName: "Another",
        lastName: "User",
        email: "devon@devonkissstudios.com",
        isActive: false,
        registered: new Date('10/08/2017 10:29:00'),
        hide: true,
      }
    ];
   }
  getUsers(): Observable<User[]> {
    console.log('Fetching users from data service...');
    return of(this.users);
  }
  addUser(user: User) {
    this.users.unshift(user);
  }
}
