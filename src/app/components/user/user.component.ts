import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  // Model Interfaces
  user: User;
  // Constructor
  constructor() {
  }
  // Angular Initializer
  ngOnInit() {
    this.user = {
      firstName: "Devon",
      lastName: "Kiss",
      email: 'example@example.com'
    }
  }
}
